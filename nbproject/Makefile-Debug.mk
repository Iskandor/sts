#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1579712804/RecSOM.o \
	${OBJECTDIR}/_ext/1579712804/SOM.o \
	${OBJECTDIR}/_ext/1579712804/SOMUtils.o \
	${OBJECTDIR}/_ext/1579712804/STS.o \
	${OBJECTDIR}/DataSet.o \
	${OBJECTDIR}/DataSetUtils.o \
	${OBJECTDIR}/LSOM.o \
	${OBJECTDIR}/MNG.o \
	${OBJECTDIR}/MSOM.o \
	${OBJECTDIR}/Neuron.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=gnu++0x
CXXFLAGS=-std=gnu++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/msom

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/msom: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/msom ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1579712804/RecSOM.o: /home/matej/Documents/Skola/rSOMs/MSOM/RecSOM.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1579712804
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1579712804/RecSOM.o /home/matej/Documents/Skola/rSOMs/MSOM/RecSOM.cpp

${OBJECTDIR}/_ext/1579712804/SOM.o: /home/matej/Documents/Skola/rSOMs/MSOM/SOM.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1579712804
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1579712804/SOM.o /home/matej/Documents/Skola/rSOMs/MSOM/SOM.cpp

${OBJECTDIR}/_ext/1579712804/SOMUtils.o: /home/matej/Documents/Skola/rSOMs/MSOM/SOMUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1579712804
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1579712804/SOMUtils.o /home/matej/Documents/Skola/rSOMs/MSOM/SOMUtils.cpp

${OBJECTDIR}/_ext/1579712804/STS.o: /home/matej/Documents/Skola/rSOMs/MSOM/STS.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1579712804
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1579712804/STS.o /home/matej/Documents/Skola/rSOMs/MSOM/STS.cpp

${OBJECTDIR}/DataSet.o: DataSet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/DataSet.o DataSet.cpp

${OBJECTDIR}/DataSetUtils.o: DataSetUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/DataSetUtils.o DataSetUtils.cpp

${OBJECTDIR}/LSOM.o: LSOM.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/LSOM.o LSOM.cpp

${OBJECTDIR}/MNG.o: MNG.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/MNG.o MNG.cpp

${OBJECTDIR}/MSOM.o: MSOM.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/MSOM.o MSOM.cpp

${OBJECTDIR}/Neuron.o: Neuron.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/Neuron.o Neuron.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=gnu++0x -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/msom

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
