#include <stdlib.h>
#include <math.h>
#include <random>
#include <assert.h>
#include <iostream>

#include "Neuron.h"


using namespace std;

Neuron::Neuron(int dim, int x, int y)
{
	weight = new float[dim];
	context = new float[dim];

	this->x = x;
	this->y = y;
	this->in_dim = dim;
  this->con_dim = dim;
  this->lat_dim = 0;

	init_weights();
}

Neuron::Neuron(int dim, int id)
{
	weight = new float[dim];
	context = new float[dim];

	this->id = id;
	this->in_dim = dim;
  this->con_dim = dim;
  this->lat_dim = 0;
  
	init_weights();
}

Neuron::Neuron(int in_dim, int con_dim, int x, int y)
{
	weight = new float[in_dim];
	context = new float[con_dim];

	this->x = x;
	this->y = y;
	this->in_dim = in_dim;
  this->con_dim = con_dim;
  this->lat_dim = 0;
  
	init_weights();  
}

Neuron::Neuron(int in_dim, int con_dim, int lat_dim, int x, int y) {
	weight = new float[in_dim];
	context = new float[con_dim];
  lateral = new float[lat_dim];
  lateral_i = new float[lat_dim];

	this->x = x;
	this->y = y;
	this->in_dim = in_dim;
  this->con_dim = con_dim;
  this->lat_dim = lat_dim;
  
	init_weights();    
}

Neuron::~Neuron(void)
{
	delete[] weight;
	delete[] context;
}

void Neuron::init_weights() {
	for(int i = 0; i < in_dim; i++) {
		weight[i] = ((float)rand()/(float)RAND_MAX)*2-1;
	}

	for(int i = 0; i < con_dim; i++) {
		context[i] = 0;
	}
  
  for(int i = 0; i < lat_dim; i++) {
    lateral[i] = 0;
    lateral_i[i] = 0;
  }
}

void Neuron::update_weights(float gama, Neuron* winner, float* xt, float* ct, float sigma, int neigh_fun) {
  float gd = 0;

  switch(neigh_fun) {
    case GAUSSIAN:
      gd = gaussian_distance(sigma, euclidian_distance(winner));
      break;
    case MEXICAN:
      gd = mexican_hat_distance(sigma, euclidian_distance(winner));
      break;
  }

	for(int i = 0; i < in_dim; i++) {
		float	dw = gama * gd * (xt[i] - weight[i]);
		weight[i]  += dw;
    if (weight[i] != weight[i]) assert(0);
	}
  for(int i = 0; i < con_dim; i++) {
    float	dc = gama * gd * (ct[i] - context[i]);
    context[i] += dc;
    if (context[i] != context[i]) assert(0);
  }
}

void Neuron::update_weights(float gama, Neuron* winner, float* xt, float sigma, int neigh_fun) {
  float gd = 0;

  switch(neigh_fun) {
    case GAUSSIAN:
      gd = gaussian_distance(sigma, euclidian_distance(winner));
      break;
    case MEXICAN:
      gd = mexican_hat_distance(sigma, euclidian_distance(winner));
      break;
  }

	for(int i = 0; i < in_dim; i++) {
		float	dw = gama * gd * (xt[i] - weight[i]);
		weight[i]  += dw;
    if (weight[i] != weight[i]) assert(0);
	}  
}

void Neuron::update_weights(float gama, float* xt, float* ct, float rnk, float sigma) {

	for(int i = 0; i < in_dim; i++) {
		float	dw = gama * exp(-rnk/sigma) * (xt[i] - weight[i]);
		weight[i]  += dw;
    if (weight[i] != weight[i]) assert(0);
	}
  for(int i = 0; i < con_dim; i++) {
    float	dc = gama * exp(-rnk/sigma) * (ct[i] - context[i]);
    context[i] += dc;
    if (context[i] != context[i]) assert(0);
  }
}

void Neuron::update_lateral_weights(float gama, Neuron** layer, float* act, float sigma, int neigh_fun, float alpha) {
  float gd = 0;
  float a;
  
  for(int i = 0; i < lat_dim; i++) {
    float pos1[2] = {(float)layer[i]->x, (float)layer[i]->y};
    float pos2[2] = {(float)x, (float)y};
    
    switch(neigh_fun) {
      case GAUSSIAN:
        gd = gaussian_distance_zero(sigma, euclidian_distance(pos1, pos2, 2));
        break;
      case MEXICAN:
        gd = mexican_hat_distance(sigma, euclidian_distance(pos1, pos2, 2));
        break;
    }

    if (gd > 0) {
      if (gd > 1) {
        assert(0);
      }
      if ((a = act[i]) > 1) {
        assert(0);
      }
      /*
      lateral[i] += gama * (alpha*gd + (1-alpha) * act[i]) * activity;
      lateral_i[i] -= gama * abs(act[i] - activity);
       * */
      float expr = (-1*pow(2, abs(act[i] - activity))) + 1;
      lateral[i] += gama * (alpha*gd + (1-alpha) * expr) * activity;
    }
  }
}

float Neuron::gaussian_distance(float sigma, float numerator) {
	float g = 0;

	g = exp(-(numerator/(2*pow(sigma, 2))));

	return g;
}

float Neuron::gaussian_distance_zero(float sigma, float numerator) {
	float g = 0;

	g = numerator != 0 ? exp(-(numerator/(2*pow(sigma, 2)))) : 0;

	return g;
}

float Neuron::mexican_hat_distance(float sigma, float numerator) {
  float m = 0;

  m = (2 * M_PI) / sqrt(sigma) * (1 - 2 * M_PI * numerator / pow(sigma, 2)) * exp(-(numerator/(pow(sigma, 2))));
  return m;
}

float Neuron::recursive_distance(float alpha, float* xt, float* ct) {
	float dt = 0;

	dt = (1 - alpha) * euclidian_distance(xt, weight, in_dim) + alpha * euclidian_distance(ct, context, con_dim);
  
  if (dt != dt) {
    dump();
    for(int i = 0; i < in_dim; i++) {
      cout << xt[i] << " ";
    }
    cout << endl;
    for(int i = 0; i < con_dim; i++) {
      cout << ct[i] << " ";
    }
    cout << endl;
    assert(0);
  }
  
  activity = exp(-sqrt(dt)/sqrt(in_dim)*5);
  
	return dt;
}

float Neuron::recursive_distance(float alpha, float beta, float* xt, float* ct) {
	float dt = 0;

	dt = alpha * euclidian_distance(xt, weight, in_dim) + beta * euclidian_distance(ct, context, con_dim);

  if (dt != dt) {
    dump();
    for(int i = 0; i < in_dim; i++) {
      cout << xt[i] << " ";
    }
    cout << endl;
    for(int i = 0; i < con_dim; i++) {
      cout << ct[i] << " ";
    }
    cout << endl;
    assert(0);
  }
  
  activity = exp(-sqrt(dt)/sqrt(in_dim)*5);
  
	return dt;  
}

float Neuron::distance(float* xt) {
  float dt = 0;
  
  dt = euclidian_distance(xt, weight, in_dim);
  
  if (dt != dt) {
    dump();
    cout << "Input:" << endl;
    for(int i = 0; i < in_dim; i++) {
      cout << xt[i] << " ";
    }
    cout << endl;
    assert(0);
  }
  
  activity = exp(-sqrt(dt)/sqrt(in_dim)*5);
  
  return dt;
}

float Neuron::euclidian_distance(float* v1, float* v2, int dim) {
	if (v1 == NULL || v2 == NULL) {
		return -1;
	}

	float sum = 0;

	for(int i = 0; i < dim; i++) {
            float x = *(v1+i);
            float y = *(v2+i);
            sum += pow((x - y), 2);
	}
	return sum;
}

float Neuron::euclidian_distance(Neuron* w) {
	if (w == NULL) {
    assert(0);
	}

	float sum = 0;

	sum += pow((float)(w->x - x), 2);
	sum += pow((float)(w->y - y), 2);

	return sum;
}

float Neuron::activate(vector<float>* input, float* matrix) {
  float act = 0.0;
  
  for(int i = 0; i < lat_dim; i++) {
    act += (*input)[i] * matrix[i];
  }
  
  return act;
}

float* Neuron::get_weight(int mode) {
  if (mode == WEIGHT) return weight;
	if (mode == CONTEXT) return context;
  if (mode == LATERAL) return lateral;
  if (mode == LATERAL_I) return lateral_i;
}

int Neuron::get_dim(int mode) {
  if (mode == WEIGHT)   return in_dim;
  if (mode == CONTEXT)  return con_dim;
  if (mode == LATERAL || mode == LATERAL_I)  return lat_dim;
}

int Neuron::get_x() {
	return x;
}

int Neuron::get_y() {
	return y;
}

void Neuron::dump() {
  cout << in_dim << endl;
  cout << con_dim << endl;
  cout << "Weights:" << endl;
  for(int i = 0; i < in_dim; i++) {
    cout << weight[i] << " ";
  }
  cout << endl;
  cout << "Context weights:" << endl;
  for(int i = 0; i < con_dim; i++) {
    cout << context[i] << " ";
  }
  cout << endl;
}