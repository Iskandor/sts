/* 
 * File:   MNC.h
 * Author: Matej Pechac
 *
 * Created on June 1, 2013, 4:04 PM
 */

#ifndef MNC_H
#define	MNC_H

class MNC {
public:
  MNC(int stsp_input_dim, int stsp_size, int motor_input_dim, int motor_size);
  MNC(const MNC& orig);
  virtual ~MNC();
private:

};

#endif	/* MNC_H */

