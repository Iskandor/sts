/* 
 * File:   SOM.h
 * Author: Matej Pechac
 *
 * Created on April 18, 2013, 9:47 PM
 */

#include <vector>
#include <iostream>
#include <fstream>
#include <string.h>

#include "DataSet.h"
#include "Neuron.h"

using namespace std;

#ifndef SOM_H
#define	SOM_H

struct sSOMTrnConfig { 
  float gama;
  int   dec_fn;
  
  sSOMTrnConfig(float _gama, int _dec_fn) {
    gama = _gama;
    dec_fn = _dec_fn;
  }
};

class SOM {
public:
  SOM(int input_dim, int map_dim_x, int map_dim_y);
  SOM(const SOM& orig);
  virtual ~SOM();
  
 	void	insert_dataset(CDataSet* data);
  void  init_training(int epochs, float gama);
	void	train(int epochs, sSOMTrnConfig config, bool loging = false);
  float	train_one_sample(vector<float> sample, int nf, bool entropy);
  void  activate(vector<float> sample, vector<float> *output, int mode);
  void  param_decay(int epoch, int mode);

	void	write_output(ofstream& myfile, vector<float> *activation);
  void  get_activation(vector<float> *output, int mode);
  void  get_size(int* size);

private:
	Neuron*	find_winner(bool entropy);
	void    sigma_decay(int t);
	void    gama_decay(int t, int mode);

private:
	CDataSet*	trn_dataset;
	Neuron**	map_layer;
	float*		input_layer;
	float		sigma, sigma0, lambda;
	float		gama, gama0, eta;

	int		input_dim, map_dim_x, map_dim_y;

};

#endif	/* SOM_H */

